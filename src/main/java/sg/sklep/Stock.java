package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Stock extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<body>\n" +
                "<h3>Stan magazynowy</h3>\n" +
                "<form action=\"stock\" method=\"POST\">" +
                "ID produktu: <input name=\"ID\" type=\"number\"/>" +
                "<br/>" +
                "Ilosc: <input name=\"quantity\" type=\"number\"/>" +
                "<br/>" +
                "Cena za sztuke: <input name=\"price\" type=\"value\"/>" +
                "<br/>" +
                "<input value=\"Dodaj produkt\" type=\"submit\"/>" +
                "</form>" +
                "</body>\n");
        out.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("ID");
        String quantity = req.getParameter("quantity");
        String price = req.getParameter("price");

        PrintWriter out = resp.getWriter();
        out.println("<html>");
        out.println("<body>\n" +
                ("<h2>Dodawanie produktu zakonczone sukcesem: </h3><br>" + "Numer porzadkowy ID: " + "<b>" + id + "</b>" +
                        "<br>" + "Dodano: " + "<b>" + quantity + " szt. </b><br>" + "Cena za sztuke: " + "<b>" + price +
                        " USD </b><br>" + "</form>" + "</body>"));
        out.println("</html>");
    }
}

// produkt ma ID oraz ilość, cena, po dodaniu info o dodaniu produktu szczegolowe, wartość sumaryczna magazynu
// kod html, response set content type


// odejmowanie ilości po zakupie przez klienta
// dodawanie ilości po dostawie
// zwrot reklamowanych produktow jako uszkodzone i wydanie dobrego produktu
